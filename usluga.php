<?php
session_start();
require_once 'includes/db.php';

$query = "SELECT * FROM `uslugi`";
$responce = mysqli_query($db, $query);
$uslugi = mysqli_fetch_all($responce, MYSQLI_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="assets\style.css">
</head>
<body>
<header>
        <div class="logo">
            <div class="logoimg">
            <a href="index.php"><img src="assets/images/logo.svg" alt="LOGO DEVIANT"></a>
            </div>
        </div>
        <nav>
        <ul>
        <?php
                if (isset($_SESSION['admin'])) {
                ?>
                        <li><a href="admin.php">Админ панель</a></li>
                <?
                }
                ?>    
            <li><a href="#">Услуги</a>
            <ul>
            <?php
                            foreach ($uslugi as $usluga) { ?>
                            <li><a href="usluga.php"><?= $usluga["name"]?></a></li>
                            <?php
                            }
                            ?>
            </ul>
            </li>
            <li><a href="#techs">Технологии</a></li>
                    <li><a href="">Портфолио</a></li>
                    <li><a href="">Команда</a></li>
                    <li><a href="">О нас</a></li>
                    <li><a href="">Стать клиентом</a></li>
        </ul>
    </nav>
    <div id="mySidenav1" class="sidenav1">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <ul>
        <?php
                if (isset($_SESSION['admin'])) {
                ?>
                        <li><a href="admin.php">Админ панель</a></li>
                <?
                }
                ?>    
            <li><a href="#">Услуги</a>
            <ul>
            <?php
                            foreach ($uslugi as $usluga) { ?>
                            <li><a href="usluga.php"><?= $usluga["name"]?></a></li>
                            <?php
                            }
                            ?>
            </ul>
            </li>
            <li><a href="#techs">Технологии</a></li>
                    <li><a href="">Портфолио</a></li>
                    <li><a href="">Команда</a></li>
                    <li><a href="">О нас</a></li>
                    <li><a href="">Стать клиентом</a></li>
        </ul>
    </div>
    
    <span onclick="openNav()"><button class="burgermenu"><svg viewBox="0 0 100 80" width="40" height="40">
        <rect width="100" height="20" fill="#00d1ff"></rect>
        <rect y="30" width="100" height="20" fill="#00d1ff"></rect>
        <rect y="60" width="100" height="20" fill="#00d1ff"></rect>
    </svg>
    </button></span>
    </header>
    <main style="margin-top:20px; margin-bottom:-22px">
        <div class="usluga1">
            <h2>Проектирование дизайн-макета</h2>
            <p><b>Проектирование дизайн-макета</b> - создание графического дизайна будущего веб-продукта в графическом редакторе.</p>
            <div class="usluga1flex">
            <div class="usluga1left">
                <img src="assets\images\usluga11.png" alt="" class="usluga1img">
                <img src="assets\images\usluga12.png" alt="" class="usluga1img">
            </div>
            <div class="usluga1right">
                <p><b>Этапы:</b><br><br>1. Договор с заказчиком о выборе среды разработки<br>дизайн-макета;<br><br>2. Составление вайрфреймов будущего веб-продукта<br>(в случае наличия вайрфреймов данный пункт упускается);<br><br><span>3. Проектирование дизайн-макетов.</span><br><br></p>
            </div>
            </div>
        </div>
    </main>
    <footer>
        саня лох
    </footer>
</body>
</html>