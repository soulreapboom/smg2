<?php
session_start();

require_once 'includes/db.php';
if(!isset($_SESSION['admin'])) {
    header('Location: /index.php');
}
$query = "SELECT * FROM `orders`";
$responce = mysqli_query($db, $query);
$orders = mysqli_fetch_all($responce, MYSQLI_ASSOC);
?>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/style.css">
    <title>Панель администратора</title>
</head>
<div id="page-preloader" class="preloader">
    <div class="loader"></div>
</div>
<body class="adminbody">
<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="adminchoiceusl.php">Услуги</a>
  <a href="adminchoiceport.php">Портфолио</a>
  <a href="adminorders.php">Заказы</a>
  <a href="adminreg.php">Регистрация нового администратора</a>
  <a href="index.php">Вернуться на главную</a>
  <a href="vendor/auth/logout.php">Выйти из аккаунта</a>
</div>
<span onclick="openNav()"><button class="openbtn">МЕНЮ</button></span>

<main class="adminmain">
    <h2>Заказы</h2>
    <div class="table">
		<div class="table-header">
			<div class="header__item"><a id="id" class="filter__link" href="#">Id</a></div>
			<div class="header__item">Имя</div>
			<div class="header__item"><a id="secondname" class="filter__link" href="#">Фамилия</a></div>
			<div class="header__item">Отчество</div>
			<div class="header__item"><a id="email" class="filter__link" href="#">Email</a></div>
			<div class="header__item"><a id="phone" class="filter__link filter__link--number" href="#">Номер телефона</a></div>
			<div class="header__item"><a id="usluga" class="filter__link filter__link--number" href="#">Usluga</a></div>
		</div>
		<div class="table-content">	
            <?php
            foreach ($orders as $order) {?>
            <div class="table-row">		
                <div class="table-data"><?= $order['id'] ?></div>
                <div class="table-data"><?= $order['name'] ?></div>
                <div class="table-data"><?= $order['secondname'] ?></div>
                <div class="table-data"><?= $order['fatherhood'] ?></div>
                <div class="table-data"><?= $order['email'] ?></div>
                <div class="table-data"><?= $order['phone'] ?></div>
                <div class="table-data"><?= $order['usluga'] ?></div>
			</div>
            <?php
            }
            ?>
		</div>	
	</div>
</main>

<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js'></script>
<script src="assets/script.js"></script>
<script>
    var properties = [
	'id',
	'secondname',
	'email',
    'phone',
    'usluga',
];

$.each( properties, function( i, val ) {
	
	var orderClass = '';

	$("#" + val).click(function(e){
		e.preventDefault();
		$('.filter__link.filter__link--active').not(this).removeClass('filter__link--active');
  		$(this).toggleClass('filter__link--active');
   		$('.filter__link').removeClass('asc desc');

   		if(orderClass == 'desc' || orderClass == '') {
    			$(this).addClass('asc');
    			orderClass = 'asc';
       	} else {
       		$(this).addClass('desc');
       		orderClass = 'desc';
       	}

		var parent = $(this).closest('.header__item');
    		var index = $(".header__item").index(parent);
		var $table = $('.table-content');
		var rows = $table.find('.table-row').get();
		var isSelected = $(this).hasClass('filter__link--active');
		var isNumber = $(this).hasClass('filter__link--number');
			
		rows.sort(function(a, b){

			var x = $(a).find('.table-data').eq(index).text();
    			var y = $(b).find('.table-data').eq(index).text();
				
			if(isNumber == true) {
    					
				if(isSelected) {
					return x - y;
				} else {
					return y - x;
				}

			} else {
			
				if(isSelected) {		
					if(x < y) return -1;
					if(x > y) return 1;
					return 0;
				} else {
					if(x > y) return -1;
					if(x < y) return 1;
					return 0;
				}
			}
    		});

		$.each(rows, function(index,row) {
			$table.append(row);
		});

		return false;
	});

});
</script>
</body>

</html>