<?php
session_start();

require_once 'includes/db.php';

$query1 = "SELECT * FROM `portfolio`";
$responce1 = mysqli_query($db, $query1);
$portfolio = mysqli_fetch_all($responce1, MYSQLI_ASSOC);

$query = "SELECT * FROM `uslugi`";
$responce = mysqli_query($db, $query);
$uslugi = mysqli_fetch_all($responce, MYSQLI_ASSOC);
?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/style.css">
    <title></title>

</head>

<body>
    <div id="page-preloader" class="preloader">
        <div class="loader"></div>
    </div>

    <header>
        <div class="logo">
            <div class="logoimg">
            <a href="index.php"><img src="assets/images/logo.svg" alt="LOGO DEVIANT"></a>
            </div>
        </div>
        <nav>
        <ul>
        <?php
                if (isset($_SESSION['admin'])) {
                ?>
                        <li><a href="admin.php">Админ панель</a></li>
                <?
                }
                ?>    
            <li><a href="#yakor1">Услуги</a>
            <ul>
            <?php
                            foreach ($uslugi as $usluga) { ?>
                            <li><a href="usluga.php"><?= $usluga["name"]?></a></li>
                            <?php
                            }
                            ?>
            </ul>
            </li>
            <li><a href="#techs">Технологии</a></li>
                    <li><a href="#yakor3">Портфолио</a></li>
                    <li><a href="#yakor4">Команда</a></li>
                    <li><a href="#yakor5">О нас</a></li>
                    <li><a href="#yakor6">Стать клиентом</a></li>
        </ul>
    </nav>
    <div id="mySidenav1" class="sidenav1">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <ul>
        <?php
                if (isset($_SESSION['admin'])) {
                ?>
                        <li><a href="admin.php">Админ панель</a></li>
                <?
                }
                ?>    
            <li><a href="#">Услуги</a>
            <ul>
            <?php
                            foreach ($uslugi as $usluga) { ?>
                            <li><a href="usluga.php"><?= $usluga["name"]?></a></li>
                            <?php
                            }
                            ?>
            </ul>
            </li>
            <li><a href="#techs">Технологии</a></li>
                    <li><a href="">Портфолио</a></li>
                    <li><a href="">Команда</a></li>
                    <li><a href="">О нас</a></li>
                    <li><a href="">Стать клиентом</a></li>
        </ul>
    </div>
    
    <span onclick="openNav()"><button class="burgermenu"><svg viewBox="0 0 100 80" width="40" height="40">
        <rect width="100" height="20" fill="#00d1ff"></rect>
        <rect y="30" width="100" height="20" fill="#00d1ff"></rect>
        <rect y="60" width="100" height="20" fill="#00d1ff"></rect>
    </svg>
    </button></span>
    </header>
    <div class="b-main-block">
    </div>
    <div class="b-bottom-block">
    </div>

    <main>
        <div class="pluses">
            <div class="plus">
                <img src="assets/images/plus1.svg" alt="" class="plusicon">
                <h2>Мы настоящие профессионалы</h2>
                <p>Наша компания существует и предоставляет услуги клиентам уже более 10 лет!</p>
            </div>
            <div class="plus">
                <img src="assets/images/plus2.svg" alt="" class="plusicon">
                <h2>У нас партнёры по всему миру</h2>
                <p>Мы сотрудничаем с крупными организациями, такими как: Google, Amazon, Microsoft.</p>
            </div>
            <div class="plus">
                <img src="assets/images/plus3.svg" alt="" class="plusicon">
                <h2>Даём гарантию на восстановление</h2>
                <p>Гарантия - 5 лет. В случае каких либо проблем мы готовы оказать содействие.</p>
            </div>
        </div>
        <div class="uslugi" id="yakor1">
            <h2>Наши услуги</h2>
            <swiper-container class="gallery swiper-container" pagination="true" effect="coverflow" grab-cursor="true" centered-slides="true" slides-per-view="auto" coverflow-effect-rotate="-10" coverflow-effect-stretch="0" coverflow-effect-depth="200" coverflow-effect-modifier="1" coverflow-effect-slide-shadows="true">
                <?php
                foreach ($uslugi as $usluga) { ?>
                    <swiper-slide class="gallery_item swiper-slide">
                        <center>
                            <img src="uploads/uslugi/<?= $usluga['img'] ?>" alt="alt">

                            <div class="textslider">
                                <h3 class="titleslider"><?= $usluga['name'] ?></h3>
                                <p class="slidetext"><?= $usluga['descsmall'] ?></p>
                            </div>
                            <span class="price"><?= $usluga['price'] ?></span><br>
                            <button class="more">Подробнее</button>
                        </center>
                    </swiper-slide>

                <?php
                }
                ?>
            </swiper-container>
        </div>
        <div class="techs" id="techs">
            <div class="picblock">
                <div class="bigpic"></div>
                <div class="smallpic1"></div>
                <div class="smallpic2"></div>
            </div>
            <div class="techblock">
                <h2>Используемые технологии</h2>
                <p class="techtext">Наша организация следит за инновациями в сфере IT. Каждый наш сотрудник является важным звеном в команде, и мы все вместе становимся лучше, собирая знания каждого в единый механизм, при этом привнося свою уникальность в различные проекты во время их реализации.</p>
                <div class="iconblock">
                    <?php
                    for ($i = 1; $i <= 12; $i++) {
                    ?>
                        <div id="icon<?= $i ?>"></div>
                    <?
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="portfolio" id="yakor3">
            <h2>Портфолио</h2>
            <div class="portfoliowrapper">
                <?php
                foreach ($portfolio as $port) { ?>
                    <div class="port">
                        <div class="portimg" id="portimg<?= $port['id'] ?>">
                            <div class="porttext">
                                <p><?php echo $port['name'] ?></p>
                            </div>
                        </div>
                    </div>
                <?
                }
                ?>
            </div>
            <center>
                <form action="portfolio.php">
                <button class="more2">Подробнее</button>
                </form>
            </center>
                
        </div>
        <div class="team" id="yakor4">
            <h2>Наша команда</h2>
            <div class="comwrapper">
                <div class="comblock">
                    <div class="teampic" style="background-image:url('assets/images/teampic1.png')"></div>
                    <h2 class="teamname">Романовский Д.А.</h2>
                    <h3 class="teamjob">Босс</h3>
                    <h3 class="teamquote">“Я металлист”</h3>
                </div>
                <div class="comblock">
                    <div class="teampic" style="background-image:url('assets/images/teampic2.png')"></div>
                    <h2 class="teamname">Скутте Д.А.</h2>
                    <h3 class="teamjob">раб1</h3>
                    <h3 class="teamquote">“Лучше быть ордынским мясом чем саней клоуном”</h3>
                </div>
                <div class="comblock">
                    <div class="teampic" style="background-image:url('assets/images/teampic3.png')"></div>
                    <h2 class="teamname">Шамшура А.И.</h2>
                    <h3 class="teamjob">раб2</h3>
                    <h3 class="teamquote">“Я клоун кста”</h3>
                </div>
            </div>
        </div>
        <div class="onas" id="yakor5">
            <h2>О нас</h2>
            <p class="onasp">Наша организация основана в 2021-ом году. До реализации веб-студии международного масштаба мы занимались обычным фрилансом. В те времена мы уже задумывались об основании команды, однако никто из нас не мог подумать, что наши масштабы разрастутся так далеко. Уже сегодня мы предоставляем услуги веб-дизайна и разработки по всему миру, и наши стремления развиваться спонсируются такими гигантами, как Google, Microsoft Corpotation, Amazon и Walmart! Смело обращайтесь в нашу организацию, если вас интересуют предоставляемые нами услуги. Мы с радостью окажем вам поддержу в реализации ваших начинаний!</p>
        </div>
        <div class="order" id="yakor6">
            <h2>Форма для заказа</h2>
            <form class="orderform" action="vendor\order.php" method="post" enctype="multipart/form-data">
                <label for="name">Имя<br>
                    <input type="text" name="name" required>
                </label>
                <label for="secondname">Фамилия<br>
                    <input type="text" name="secondname" required>
                </label>
                <label for="fatherhood">Отчество (если имеется)<br>
                    <input type="text" name="fatherhood" required>
                </label>
                <label for="email">Email<br>
                    <input type="email" name="email" required>
                </label>
                <label for="phone">Номер телефона<br>
                    <input placeholder="+7(XXX) XXX-XX-XX" name="phone" id="phone" type="tel" pattern="(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?" title="Введите номер телефона в формате +X XXX XXX XX XX" required>
                </label>
                <label for="usluga">Услуга<br>
                    <select name="usluga">
                        <?php foreach ($uslugi as $usluga) { ?>
                            <option name="<?= $usluga['name'] ?>" value="<?= $usluga['id'] ?>"><?=$usluga['name'] ?></option>
                        <?
                        }
                        ?>
                    </select>
                </label>
                <button class="more3">Подтвердить заказ</button>
            </form>
        </div>
    </main>
    <footer>
        (С) Copyright
    </footer>
    <script src="assets\jquery.maskedinput.js"></script>
    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js'></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-element-bundle.min.js"></script>
    <script src="assets/script.js" async></script>
    <script>
[].forEach.call(document.querySelectorAll('input[type="tel"]'), function (input) {
    let keyCode;
    function mask(event) {
      event.keyCode && (keyCode = event.keyCode);
      let pos = this.selectionStart;
      if (pos < 3) event.preventDefault();
      let matrix = '+7 (___) ___-__-__',
        i = 0,
        def = matrix.replace(/\D/g, ''),
        val = this.value.replace(/\D/g, ''),
        new_value = matrix.replace(/[_\d]/g, function (a) {
          return i < val.length ? val.charAt(i++) || def.charAt(i) : a
        });
      i = new_value.indexOf('_');
      if (i != -1) {
        i < 5 && (i = 3);
        new_value = new_value.slice(0, i)
      }
      var reg = matrix.substr(0, this.value.length).replace(/_+/g,
        function (a) {
          return '\\d{1,' + a.length + '}'
        }).replace(/[+()]/g, '\\$&');
      reg = new RegExp('^' + reg + '$');
      if (!reg.test(this.value) || this.value.length < 5 || keyCode > 47 && keyCode < 58) this.value = new_value;
      if (event.type == 'blur' && this.value.length < 5) this.value = ''
    }
    input.addEventListener('input', mask, false);
    input.addEventListener('focus', mask, false);
    input.addEventListener('blur', mask, false);
    input.addEventListener('keydown', mask, false);
  });
    </script>
    <style>
        <?php
        for ($i = 1; $i <= 12; $i++) {
            echo "#icon$i" ?> {
            background-image: url('assets/images/icon<?= $i ?>.svg');
            transition-duration: 0.2s;
        }

        <? echo "#icon$i" ?>:hover {
            background-image: url('assets/images/iconback<?= $i ?>.svg');
        }

        <?
        }
        foreach ($portfolio as $port) {
            $portimg = $port['id'];
            echo "#portimg$portimg" ?> {
            background-image: url('uploads/portfolio/<?= $port['img'] ?>')
        }

        <?
        }
        ?>
    </style>
</body>

</html>