<?php
session_start();

require_once 'includes/db.php';
if(!isset($_SESSION['admin'])) {
    header('Location: /index.php');
}
$query = "SELECT * FROM `portfolio`";
$responce = mysqli_query($db, $query);
$portfolio = mysqli_fetch_all($responce, MYSQLI_ASSOC);
?>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/style.css">
    <title>Панель администратора</title>
</head>
<div id="page-preloader" class="preloader">
    <div class="loader"></div>
</div>
<body class="adminbody">
<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="adminchoiceusl.php">Услуги</a>
  <a href="adminchoiceport.php">Портфолио</a>
  <a href="adminorders.php">Заказы</a>
  <a href="adminreg.php">Регистрация нового администратора</a>
  <a href="index.php">Вернуться на главную</a>
  <a href="vendor/auth/logout.php">Выйти из аккаунта</a>
</div>
<span onclick="openNav()"><button class="openbtn">МЕНЮ</button></span>

<main class="adminmain">
    <h2>Добавить в Портфолио</h2>
    <span>
    <a href="adminaddport.php"><button class="adminbtn" >Добавить</button></a>
    </span>
    <h2>Редактировать Портфолио</h2>
    <span>
    <form action="vendor/selecteditem2.php" method="post" enctype="multipart/form-data" class="btnform">
    <select name="update">
        <?php
            foreach ($portfolio as $port) { ?>
                <option name="updateopt" value="<?= $port['id'] ?>"><?= $port['id'] . ' ' . $port['name'] ?></option>
                <?
            }
                ?>
    </select>
    <button class="adminbtn">Редактировать</button></form>
    </span>
    <h2>Удалить в Портфолио</h2>
    <form action="vendor/selecteditem3.php" method="post" enctype="multipart/form-data" class="btnform">
    <select name="delete">
    <?php
            foreach ($portfolio as $port) { ?>
                <option name="updateopt" value="<?= $port['id'] ?>"><?= $port['id'] . ' ' . $port['name'] ?></option>
                <?
            }
                ?>
    </select>
    <button class="adminbtn">Удалить</button>
    </form>
</main>
    
</body>
<script src="assets/script.js"></script>
</html>