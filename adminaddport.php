<?php
session_start();

if(!isset($_SESSION['admin'])) {
    header('Location: /index.php');
}
?>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/style.css">
    <title>Панель администратора</title>
</head>
<div id="page-preloader" class="preloader">
            <div class="loader"></div>
    </div>
<body class="adminbody">
<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="adminchoiceusl.php">Услуги</a>
  <a href="adminchoiceport.php">Портфолио</a>
  <a href="adminorders.php">Заказы</a>
  <a href="adminreg.php">Регистрация нового администратора</a>
  <a href="index.php">Вернуться на главную</a>
  <a href="vendor/auth/logout.php">Выйти из аккаунта</a>
</div>
<span onclick="openNav()"><button class="openbtn">МЕНЮ</button></span>
<main class="adminmain">
    <h2>Добавить Портфолио</h2>
    
    <form action="vendor\addport.php" method="post" enctype="multipart/form-data" class="addform" style="margin-top:30px;display:flex; align-items:center; flex-direction:column">
    <div class="slidercard" style="width:580px; height:440px; ">
        <div class="portblock" style="width:100%;display:flex; flex-direction:column; align-items:center">
            <label class="inputfile" style="">
                <input type="file" name="image" required>
                <span>Добавить изображение</span>
            </label>
            <label for="name">
                <input class="centername" type="text" placeholder="Добавить название" name="name" required>
            </label>
        </div>
</div>
    <button class="send">Создать Портфолио</button>
    </form>
</div>
</main>
</body>
<script src="assets/script.js"></script>
<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js'></script>

</html>