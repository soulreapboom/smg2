<?php
session_start();

require_once '../../includes/db.php';

unset($_SESSION['admin']);
header('Location: /index.php');
?>