<?php
session_start();

if(isset($_SESSION['user'])) {
    header('Location: /index.php');
}
require_once '../../includes/db.php';

$login = $_POST['login'];
$password = $_POST['password'];
$repeatPassword = $_POST['passwordrepeat'];

if($password === $repeatPassword) {
    $query = "INSERT INTO `admins` (`id`,`login`,`password`)VALUES(NULL,'$login','$password')";

    $response = mysqli_query($db, $query);

    if($response) {
        unset ($_SESSION['validation']);
        unset ($_SESSION['message']);

        header('Location: /adminreg.php');
    }
    else {
        $_SESSION['message'] = [
            'type' => 'error',
            'text' => 'Register error'

        ];
    
    header('Location: /adminreg.php');
    }

}
else {
    $_SESSION['validation'] = [
        'Password' => 'Password do not match',
        'RepeatPassword' => 'Password do not match'
    ];

    header('Location: /adminreg.php');
}
