<?php
session_start();

require_once '../../includes/db.php';

$login = $_POST['login'];
$password = $_POST['password'];

$query = "SELECT * FROM `admins` WHERE (`login` = '$login' AND `password` = '$password') LIMIT 1";
$response = mysqli_query($db, $query);

if (mysqli_num_rows($response) === 0) {
    $_SESSION['message'] = [
        'type' => 'error',
        'text' => 'Incorrect login or password'
    ];

    header('Location: /adminlogin.php');
} else {
    $result = mysqli_fetch_assoc($response);
    $_SESSION['admin'] = [
        'id' => $result['id'],
        'login'=> $result['login'],
        'password' => $result['password']
    ];

    unset($_SESSION['message']);
    unset($_SESSION['validation']);

    header('Location: ../../admin.php');
}
?>