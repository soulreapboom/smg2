<?php
session_start();

if(!isset($_SESSION['admin'])) {
    header('Location: /index.php');
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/style.css">
    <title>Панель администратора</title>
</head>
    <div id="page-preloader" class="preloader">
            <div class="loader"></div>
    </div>
<body class="adminbody">
<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="adminchoiceusl.php">Услуги</a>
  <a href="adminchoiceport.php">Портфолио</a>
  <a href="adminorders.php">Заказы</a>
  <a href="adminreg.php">Регистрация нового администратора</a>
  <a href="index.php">Вернуться на главную</a>
  <a href="vendor/auth/logout.php">Выйти из аккаунта</a>
</div>

<span onclick="openNav()"><button class="openbtn">МЕНЮ ПАНЕЛИ</button></span>

    <div class="regblock">
        <div class="logologin">
            <img src="assets/images/pissy.jpg" alt="">
        </div>
            <p style="margin-top: -20px; margin-bottom:80px;">Регистрация нового администратора</p>
            <form action="vendor/auth/registration.php" method="post" style="margin-left:50px">
                <label for="login"><b>Login</b><br>
                <input type="text" placeholder="login" name="login" required><br><br>
                </label>
                <label for="password"><b>Пароль</b><br>
                <input type="password" placeholder="Введите пароль" name="password" required><br><br>
                </label>
                <label for="passwordrepeat"><b>Повторите пароль</b><br>
                <input type="password" placeholder="Введите пароль" name="passwordrepeat" required><br><br>
                </label>
                <div class="flexcontainer">
                    <button type="submit" class="buttonnnn" style="margin-left: -150px;">Зарегистрировать</button>
                </div>
            </form>
    </div>

<script src="assets/script.js"></script>
</body>
</html>