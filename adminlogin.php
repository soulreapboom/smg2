<?php
session_start();

if (isset($_SESSION['admin'])) {
    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/style.css">
    <title>Document</title>
</head>
    <div id="page-preloader" class="preloader">
        <div class="loader"></div>
    </div>
<body>
    <?php

    if (isset($_SESSION['message'])) {
    ?>
        <div class="message <?= $_SESSION['message']['type'] ?>">
            <p class="message-text"><?= $_SESSION['message']['text'] ?></p>
        </div>
    <?php
    }
    ?>
    <div class="backblock">
        <div class="block1">
    <div class="logologin">
        <img src="assets/images/pissy.jpg" alt="">
    </div>
        <p style="margin-top: -20px; margin-bottom:80px">Авторизация в панель администратора</p>
        <form action="vendor/auth/login.php" method="post">
            <label for="login"><b>Login</b><br>
            <input type="text" placeholder="login" name="login" required><br><br>
            </label>
            <label for="password"><b>Пароль</b><br>
            <input type="password" placeholder="Введите пароль" name="password" required><br><br>
            </label>
            <div class="flexcontainer">
                <button type="submit" class="buttonnnn" style="margin-left: -110px;">Авторизироваться</button>
            </div>
        </form>
        <div class="buttonex"><a href="index.html"><img src="assets/images/leftarrow1.svg" alt=""></div></button>
    </div>
    </div>
<script src="assets/script.js"></script>
</body>
</html>