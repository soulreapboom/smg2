<?php
session_start();

require_once 'includes/db.php';

$query1 = "SELECT * FROM `portfolio`";
$responce1 = mysqli_query($db, $query1);
$portfolio = mysqli_fetch_all($responce1, MYSQLI_ASSOC);

$query = "SELECT * FROM `uslugi`";
$responce = mysqli_query($db, $query);
$uslugi = mysqli_fetch_all($responce, MYSQLI_ASSOC);
?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/style.css">
    <title></title>

</head>

<body>
    <div id="page-preloader" class="preloader">
        <div class="loader"></div>
    </div>

    <header>
        <div class="logo">
            <div class="logoimg">
            <a href="index.php"><img src="assets/images/logo.svg" alt="LOGO DEVIANT"></a>
            </div>
        </div>
        <nav>
        <ul>
        <?php
                if (isset($_SESSION['admin'])) {
                ?>
                        <li><a href="admin.php">Админ панель</a></li>
                <?
                }
                ?>    
            <li><a href="#">Услуги</a>
            <ul>
            <?php
                            foreach ($uslugi as $usluga) { ?>
                            <li><a href="usluga.php"><?= $usluga["name"]?></a></li>
                            <?php
                            }
                            ?>
            </ul>
            </li>
            <li><a href="#techs">Технологии</a></li>
                    <li><a href="">Портфолио</a></li>
                    <li><a href="">Команда</a></li>
                    <li><a href="">О нас</a></li>
                    <li><a href="">Стать клиентом</a></li>
        </ul>
    </nav>
    <div id="mySidenav1" class="sidenav1">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <ul>
        <?php
                if (isset($_SESSION['admin'])) {
                ?>
                        <li><a href="admin.php">Админ панель</a></li>
                <?
                }
                ?>    
            <li><a href="#">Услуги</a>
            <ul>
            <?php
                            foreach ($uslugi as $usluga) { ?>
                            <li><a href="usluga.php"><?= $usluga["name"]?></a></li>
                            <?php
                            }
                            ?>
            </ul>
            </li>
            <li><a href="#techs">Технологии</a></li>
                    <li><a href="">Портфолио</a></li>
                    <li><a href="">Команда</a></li>
                    <li><a href="">О нас</a></li>
                    <li><a href="">Стать клиентом</a></li>
        </ul>
    </div>
    
    <span onclick="openNav()"><button class="burgermenu"><svg viewBox="0 0 100 80" width="40" height="40">
        <rect width="100" height="20" fill="#00d1ff"></rect>
        <rect y="30" width="100" height="20" fill="#00d1ff"></rect>
        <rect y="60" width="100" height="20" fill="#00d1ff"></rect>
    </svg>
    </button></span>
    </header>
    <main class="notmain">
                <p>На данной странице вы можете ознакомиться со всеми нашими выполнеными заказами</p>
                <div class="portfolio">
            <h2>Портфолио</h2>
            <div class="portfoliowrapper">
                <?php
                foreach ($portfolio as $port) { ?>
                    <div class="port">
                        <div class="portimg" id="portimg<?= $port['id'] ?>">
                            <div class="porttext">
                                <p><?php echo $port['name'] ?></p>
                            </div>
                        </div>
                    </div>
                <?
                }
                ?>
            </div>
        </div>
    </main>
    <footer id="yakor1">
        (С) Copyright
    </footer>
    <style>
        <?php
        foreach ($portfolio as $port) {
            $portimg = $port['id'];
            echo "#portimg$portimg" ?> {
            background-image: url('uploads/portfolio/<?= $port['img'] ?>')
        }

        <?
        }
        ?>
    </style>
    <script src="assets/script.js"></script>
    <script src="assets\jquery.maskedinput.js"></script>
    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js'></script>
</body>
</html>